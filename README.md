# satisfactory

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/satisfactory?style=plastic)](https://bitbucket.org/idomdavis/satisfactory/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/satisfactory?style=plastic)](https://bitbucket.org/idomdavis/satisfactory/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/satisfactory?style=plastic)](https://bitbucket.org/idomdavis/satisfactory/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/satisfactory)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

[Satisfactory](https://www.satisfactorygame.com) Production Tools. 
Data has been copied from 
[Satisfactory Calculator](https://satisfactory-calculator.com). Very much a work 
in progress for planning my own factory. Presented here for others to play with.

## Usage:

Go look at `layout.go`, fiddle with it, run it.

## Known Issues

* The sort algorithm on Steps has problems.
* Documentation is poor.
* Testing is non-existent.
