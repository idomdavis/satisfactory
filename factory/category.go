package factory

// Category a Product belongs to
type Category string

// Product categories
const (
	Ores       = Category("ores")
	Liquids    = Category("liquids")
	Materials  = Category("materials")
	Components = Category("components")
	Fuels      = Category("fuels")
	Ammo       = Category("ammo")
	Special    = Category("special")
	Organic    = Category("organic")
)
