package factory

// Product in the factory
type Product struct {
	Name     string
	Points   int
	Modifier float64

	Category
}

// Modified returns a modified points cost for the product.
func (p Product) Modified() float64 {
	return float64(p.Points) * p.Modifier
}

// The set of products.
//nolint:gomnd,misspell
var (
	Limestone = Product{
		Category: Ores,
		Name:     "Limestone",
		Points:   2,
		Modifier: 1,
	}
	IronOre = Product{
		Category: Ores,
		Name:     "Iron Ore",
		Points:   1,
		Modifier: 1,
	}
	CopperOre = Product{
		Category: Ores,
		Name:     "Copper Ore",
		Points:   3,
		Modifier: 1,
	}
	CateriumOre = Product{
		Category: Ores,
		Name:     "Caterium Ore",
		Points:   7,
		Modifier: 1,
	}
	Coal = Product{
		Category: Ores,
		Name:     "Coal",
		Points:   3,
		Modifier: 1,
	}
	RawQuartz = Product{
		Category: Ores,
		Name:     "Raw Quartz",
		Points:   15,
		Modifier: 1,
	}
	Sulfur = Product{
		Category: Ores,
		Name:     "Sulfur",
		Points:   11,
		Modifier: 1,
	}
	Bauxite = Product{
		Category: Ores,
		Name:     "Bauxite",
		Points:   8,
		Modifier: 1,
	}
	Uranium = Product{
		Category: Ores,
		Name:     "Uranium",
		Points:   35,
		Modifier: 1,
	}

	Water = Product{
		Category: Liquids,
		Name:     "Water",
		Points:   5,
		Modifier: 1,
	}
	CrudeOil = Product{
		Category: Liquids,
		Name:     "Crude Oil",
		Points:   30,
		Modifier: 1,
	}
	HeavyOilResidue = Product{
		Category: Liquids,
		Name:     "Heavy Oil Residue",
		Points:   30,
		Modifier: 1,
	}
	Fuel = Product{
		Category: Liquids,
		Name:     "Fuel",
		Points:   75,
		Modifier: 1,
	}
	Turbofuel = Product{
		Category: Liquids,
		Name:     "Turbofuel",
		Points:   225,
		Modifier: 1,
	}
	AluminaSolution = Product{
		Category: Liquids,
		Name:     "Alumina Solution",
		Points:   10,
		Modifier: 1,
	}
	SulfuricAcid = Product{
		Category: Liquids,
		Name:     "Sulfuric Acid",
		Points:   1,
		Modifier: 1,
	}

	Concrete = Product{
		Category: Materials,
		Name:     "Concrete",
		Points:   12,
		Modifier: 1,
	}
	IronIngot = Product{
		Category: Materials,
		Name:     "Iron Ingot",
		Points:   2,
		Modifier: 1,
	}
	CopperIngot = Product{
		Category: Materials,
		Name:     "Copper Ingot",
		Points:   6,
		Modifier: 1,
	}
	CateriumIngot = Product{
		Category: Materials,
		Name:     "Caterium Ingot",
		Points:   42,
		Modifier: 1,
	}
	SteelIngot = Product{
		Category: Materials,
		Name:     "Steel Ingot",
		Points:   8,
		Modifier: 1,
	}
	AluminumIngot = Product{
		Category: Materials,
		Name:     "Aluminum Ingot",
		Points:   190,
		Modifier: 1,
	}
	QuartzCrystal = Product{
		Category: Materials,
		Name:     "Quartz Crystal",
		Points:   50,
		Modifier: 1,
	}
	PolymerResin = Product{
		Category: Materials,
		Name:     "Polymer Resin",
		Points:   12,
		Modifier: 1,
	}
	PetroleumCoke = Product{
		Category: Materials,
		Name:     "Petroleum Coke",
		Points:   20,
		Modifier: 1,
	}
	AluminumScrap = Product{
		Category: Materials,
		Name:     "Aluminum Scrap",
		Points:   20,
		Modifier: 1,
	}
	Silica = Product{
		Category: Materials,
		Name:     "Silica",
		Points:   20,
		Modifier: 1,
	}
	BlackPowder = Product{
		Category: Materials,
		Name:     "Black Powder",
		Points:   50,
		Modifier: 1,
	}
	Wire = Product{
		Category: Materials,
		Name:     "Wire",
		Points:   6,
		Modifier: 1,
	}
	Cable = Product{
		Category: Materials,
		Name:     "Cable",
		Points:   24,
		Modifier: 1,
	}
	IronRod = Product{
		Category: Materials,
		Name:     "Iron Rod",
		Points:   4,
		Modifier: 1,
	}
	Screw = Product{
		Category: Materials,
		Name:     "Screw",
		Points:   2,
		Modifier: 1,
	}
	IronPlate = Product{
		Category: Materials,
		Name:     "Iron Plate",
		Points:   6,
		Modifier: 1,
	}
	ReinforcedIronPlate = Product{
		Category: Materials,
		Name:     "Reinforced Iron Plate",
		Points:   120,
		Modifier: 1,
	}
	CopperSheet = Product{
		Category: Materials,
		Name:     "Copper Sheet",
		Points:   24,
		Modifier: 1,
	}
	AlcladAluminumSheet = Product{
		Category: Materials,
		Name:     "Alclad Aluminum Sheet",
		Points:   769,
		Modifier: 1,
	}
	Plastic = Product{
		Category: Materials,
		Name:     "Plastic",
		Points:   75,
		Modifier: 1,
	}
	Rubber = Product{
		Category: Materials,
		Name:     "Rubber",
		Points:   60,
		Modifier: 1,
	}
	SteelPipe = Product{
		Category: Materials,
		Name:     "Steel Pipe",
		Points:   24,
		Modifier: 1,
	}
	SteelBeam = Product{
		Category: Materials,
		Name:     "Steel Beam",
		Points:   64,
		Modifier: 1,
	}
	EncasedIndustrialBeam = Product{
		Category: Materials,
		Name:     "Encased Industrial Beam",
		Points:   632,
		Modifier: 1,
	}
	GreenPowerSlug = Product{
		Category: Materials,
		Name:     "Green Power Slug",
		Points:   0,
		Modifier: 1,
	}
	YellowPowerSlug = Product{
		Category: Materials,
		Name:     "Yellow Power Slug",
		Points:   0,
		Modifier: 1,
	}
	PurplePowerSlug = Product{
		Category: Materials,
		Name:     "Purple Power Slug",
		Points:   0,
		Modifier: 1,
	}
	FlowerPetals = Product{
		Category: Materials,
		Name:     "Flower Petals",
		Points:   10,
		Modifier: 1,
	}

	CrystalOscillator = Product{
		Category: Components,
		Name:     "Crystal Oscillator",
		Points:   3072,
		Modifier: 1,
	}
	EmptyCanister = Product{
		Category: Components,
		Name:     "Empty Canister",
		Points:   60,
		Modifier: 1,
	}
	Fabric = Product{
		Category: Components,
		Name:     "Fabric",
		Points:   140,
		Modifier: 1,
	}
	ModularFrame = Product{
		Category: Components,
		Name:     "Modular Frame",
		Points:   408,
		Modifier: 1,
	}
	HeavyModularFrame = Product{
		Category: Components,
		Name:     "Heavy Modular Frame",
		Points:   11520,
		Modifier: 1,
	}
	Rotor = Product{
		Category: Components,
		Name:     "Rotor",
		Points:   140,
		Modifier: 1,
	}
	Stator = Product{
		Category: Components,
		Name:     "Stator",
		Points:   240,
		Modifier: 1,
	}
	Motor = Product{
		Category: Components,
		Name:     "Motor",
		Points:   1520,
		Modifier: 1,
	}
	Quickwire = Product{
		Category: Components,
		Name:     "Quickwire",
		Points:   17,
		Modifier: 1,
	}
	CircuitBoard = Product{
		Category: Components,
		Name:     "Circuit Board",
		Points:   696,
		Modifier: 1,
	}
	Computer = Product{
		Category: Components,
		Name:     "Computer",
		Points:   17260,
		Modifier: 1,
	}
	AILimiter = Product{
		Category: Components,
		Name:     "A.I. Limiter",
		Points:   920,
		Modifier: 1,
	}
	HighSpeedConnector = Product{
		Category: Components,
		Name:     "High Speed Connector",
		Points:   3776,
		Modifier: 1,
	}
	Supercomputer = Product{
		Category: Components,
		Name:     "Supercomputer",
		Points:   99576,
		Modifier: 1,
	}
	Battery = Product{
		Category: Components,
		Name:     "Battery",
		Points:   4712,
		Modifier: 1,
	}
	HeatSink = Product{
		Category: Components,
		Name:     "Heat Sink",
		Points:   6992,
		Modifier: 1,
	}
	RadioControlUnit = Product{
		Category: Components,
		Name:     "Radio Control Unit",
		Points:   98520,
		Modifier: 1,
	}
	TurboMotor = Product{
		Category: Components,
		Name:     "Turbo Motor",
		Points:   465056,
		Modifier: 1,
	}
	ElectromagneticControlRod = Product{
		Category: Components,
		Name:     "Electromagnetic Control Rod",
		Points:   2560,
		Modifier: 1,
	}
	UraniumPellet = Product{
		Category: Components,
		Name:     "Uranium Pellet",
		Points:   112,
		Modifier: 1,
	}
	EncasedUraniumCell = Product{
		Category: Components,
		Name:     "Encased Uranium Cell",
		Points:   918,
		Modifier: 1,
	}
	Beacon = Product{
		Category: Components,
		Name:     "Beacon",
		Points:   320,
		Modifier: 1,
	}

	CompactedCoal = Product{
		Category: Fuels,
		Name:     "Compacted Coal",
		Points:   28,
		Modifier: 1,
	}
	Biomass = Product{
		Category: Fuels,
		Name:     "Biomass",
		Points:   12,
		Modifier: 1,
	}
	SolidBiofuel = Product{
		Category: Fuels,
		Name:     "Solid Biofuel",
		Points:   48,
		Modifier: 1,
	}
	PackagedTurbofuel = Product{
		Category: Fuels,
		Name:     "Packaged Turbofuel",
		Points:   570,
		Modifier: 1,
	}
	NuclearFuelRod = Product{
		Category: Fuels,
		Name:     "Nuclear Fuel Rod",
		Points:   75292,
		Modifier: 1,
	}

	Nobelisk = Product{
		Category: Ammo,
		Name:     "Nobelisk",
		Points:   980,
		Modifier: 1,
	}
	GasFilter = Product{
		Category: Ammo,
		Name:     "Gas Filter",
		Points:   830,
		Modifier: 1,
	}
	ColorCartridge = Product{
		Category: Ammo,
		Name:     "Color Cartridge",
		Points:   10,
		Modifier: 1,
	}
	RifleCartridge = Product{
		Category: Ammo,
		Name:     "Rifle Cartridge",
		Points:   664,
		Modifier: 1,
	}
	SpikedRebar = Product{
		Category: Ammo,
		Name:     "Spiked Rebar",
		Points:   8,
		Modifier: 1,
	}
	IodineInfusedFilter = Product{
		Category: Ammo,
		Name:     "Iodine Infused Filter",
		Points:   2172,
		Modifier: 1,
	}

	PowerShard = Product{
		Category: Special,
		Name:     "Power Shard",
		Points:   0,
		Modifier: 1,
	}
	SmartPlating = Product{
		Category: Special,
		Name:     "Smart Plating",
		Points:   520,
		Modifier: 1,
	}
	VersatileFramework = Product{
		Category: Special,
		Name:     "Versatile Framework",
		Points:   1176,
		Modifier: 1,
	}
	AutomatedWiring = Product{
		Category: Special,
		Name:     "Automated Wiring",
		Points:   1440,
		Modifier: 1,
	}
	ModularEngine = Product{
		Category: Special,
		Name:     "Modular Engine",
		Points:   9960,
		Modifier: 1,
	}
	AdaptiveControlUnit = Product{
		Category: Special,
		Name:     "Adaptive Control Unit",
		Points:   86120,
		Modifier: 1,
	}

	AlienCarapace = Product{
		Category: Organic,
		Name:     "Alien Carapace",
		Points:   0,
		Modifier: 1,
	}
	AlienOrgans = Product{
		Category: Organic,
		Name:     "Alien Organs",
		Points:   0,
		Modifier: 1,
	}
	Leaves = Product{
		Category: Organic,
		Name:     "Leaves",
		Points:   3,
		Modifier: 1,
	}
	Wood = Product{
		Category: Organic,
		Name:     "Wood",
		Points:   30,
		Modifier: 1,
	}
	Mycelia = Product{
		Category: Organic,
		Name:     "Mycelia",
		Points:   10,
		Modifier: 1,
	}
)
