package factory_test

import (
	"fmt"

	"bitbucket.org/idomdavis/satisfactory/factory"
)

func ExampleProduceAll() {
	p := factory.ProduceAll(factory.Fastest{})

	fmt.Println(p.Power(), "MW")

	// Output:
	// 2261 MW
}
