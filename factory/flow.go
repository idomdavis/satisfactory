package factory

// Flow of Product (items/minute).
type Flow struct {
	Product
	Rate float64
}
