package factory

import "fmt"

// Step in a Plan. Each step uses a Recipe and will require Count Producer types
// to be properly implemented.
type Step struct {
	Count int
	Recipe
}

// Steps is a set of Step types.
type Steps []Step

func (s Step) String() string {
	return fmt.Sprintf("%d x %s", s.Count, s.Recipe.String())
}

// Len returns the number of Steps.
func (s Steps) Len() int {
	return len(s)
}

// Swap does an in place swap of two steps.
func (s Steps) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Less returns true if i is "less than" j.
func (s Steps) Less(i, j int) bool {
	l := s[i]
	r := s[j]

	if l.Order >= r.Order {
		return false
	}

	for _, input := range l.Inputs {
		for _, output := range r.Outputs {
			if input == output {
				return false
			}
		}
	}

	return true
}
