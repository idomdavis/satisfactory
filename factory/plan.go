package factory

import (
	"sort"
	"strings"
)

// Plan for a factory.
type Plan struct {
	Steps    Steps
	Capacity map[Product]float64

	Comparator
}

// Produce a plan for the given Product set using the provided comparator to
// choose recipes.
func Produce(products []Product, comparator Comparator) Plan {
	p := Plan{Comparator: comparator, Capacity: map[Product]float64{}}

	for _, product := range products {
		p.Require(Flow{Product: product, Rate: 1})
	}

	sort.Sort(p.Steps)

	return p
}

// ProduceAll produces a plan for all products that are not used as an input for
// anything else.
func ProduceAll(comparator Comparator) Plan {
	return Produce(Terminal(), comparator)
}

// Require a Flow in the plan.
func (p *Plan) Require(flow Flow) {
	p.Capacity[flow.Product] -= flow.Rate

	if p.Capacity[flow.Product] < 0 {
		p.Produce(flow.Product)
	}
}

// Produce a Product in the plan.
func (p *Plan) Produce(product Product) {
	var found bool

	recipe := p.Best(product, RecipesFor(product))

	for i, s := range p.Steps {
		if s.Recipe.Name == recipe.Name {
			found = true
			p.Steps[i].Count++
		}
	}

	if !found {
		p.Steps = append(p.Steps, Step{Count: 1, Recipe: recipe})
	}

	for _, output := range recipe.Outputs {
		p.Capacity[output.Product] += output.Rate
	}

	for _, input := range recipe.Inputs {
		p.Require(input)
	}
}

// Lanes returns the set of non Ore items required for calculating the number
// of conveyor lanes required.
func (p *Plan) Lanes() []string {
	var lanes []string

	for k := range p.Capacity {
		if k.Category != Liquids && k.Category != Ores {
			lanes = append(lanes, k.Name)
		}
	}

	return lanes
}

// Power returns the total power required by the Plan in Megawatts.
func (p *Plan) Power() int {
	var power int

	for _, s := range p.Steps {
		power += s.Power
	}

	return power
}

func (p *Plan) String() string {
	steps := make([]string, len(p.Steps))

	for i, s := range p.Steps {
		steps[i] = s.String()
	}

	return strings.Join(steps, "\n")
}
