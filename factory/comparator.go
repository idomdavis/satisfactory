package factory

// A Comparator is used to determine the best Recipe for a Product.
type Comparator interface {
	Best(product Product, recipes []Recipe) Recipe
}

// Fastest Comparator picks the Recipe with the fastest rate of output.
type Fastest struct{}

// Best returns the recipe with the fasted output.
func (f Fastest) Best(product Product, recipes []Recipe) Recipe {
	var (
		best Recipe
		rate float64
	)

	for _, r := range recipes {
		for _, o := range r.Outputs {
			if o.Product == product && o.Rate > rate {
				rate = o.Rate
				best = r
			}
		}
	}

	return best
}

// NonOrganic is a filter that strips out all recipes requiring items from the
// Organic category, passing the filtered list to the nested comparator.
type NonOrganic struct {
	Comparator Comparator
}

// Best returns the output of the nested Comparator, after filtering it for
// Organics.
func (n NonOrganic) Best(product Product, recipes []Recipe) Recipe {
	var allowed []Recipe

	for _, recipe := range recipes {
		var organic bool
		for _, input := range recipe.Inputs {
			organic = organic || input.Category == Organic
		}

		if !organic {
			allowed = append(allowed, recipe)
		}
	}

	return n.Comparator.Best(product, allowed)
}
