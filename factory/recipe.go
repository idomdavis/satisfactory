package factory

import (
	"fmt"
	"strings"
)

// A Recipe describes how to produce Product types.
type Recipe struct {
	Name string
	Producer
	Inputs  []Flow
	Outputs []Flow
}

// Rates for miners.
const (
	//Impure = 60
	Normal = 120
	Pure   = 240
)

// Terminal produces a list of Product types that are not required as an input
// for any other recipe. Using Terminal() as the input for a factory should, in
// theory, produce everything in game. In practice it's possible to avoid
// certain Products so the factory may not Produce the complete set of products.
func Terminal() []Product {
	var results []Product

	terminal := map[Product]struct{}{}

	for _, r := range Recipes {
		for _, o := range r.Outputs {
			terminal[o.Product] = struct{}{}
		}
	}

	for _, r := range Recipes {
		for _, i := range r.Inputs {
			delete(terminal, i.Product)
		}
	}

	for k := range terminal {
		results = append(results, k)
	}

	return results
}

// RecipesFor returns the set of Recipe types that can be used to produce the
// given Product.
func RecipesFor(product Product) []Recipe {
	var results []Recipe

	for _, r := range Recipes {
		for _, o := range r.Outputs {
			if o.Product == product {
				results = append(results, r)
			}
		}
	}

	return results
}

func (r Recipe) String() string {
	in := make([]string, len(r.Inputs))
	out := make([]string, len(r.Outputs))

	for i, input := range r.Inputs {
		in[i] = input.Name
	}

	for i, output := range r.Outputs {
		out[i] = output.Name
	}

	return fmt.Sprintf("(%s)->[%s]->(%s) : %s", strings.Join(in, ", "),
		r.Producer.Name, strings.Join(out, ", "), r.Name)
}

// Recipes available to the factory.
//nolint:gomnd,misspell
var Recipes = []Recipe{
	{
		Name:     "Leaves",
		Producer: Nature,
		Outputs:  []Flow{{Product: Leaves, Rate: 1}},
	},
	{
		Name:     "Wood",
		Producer: Nature,
		Outputs:  []Flow{{Product: Wood, Rate: 1}},
	},
	{
		Name:     "Mycelia",
		Producer: Nature,
		Outputs:  []Flow{{Product: Mycelia, Rate: 1}},
	},
	{
		Name:     "Green Power Slug",
		Producer: Nature,
		Outputs:  []Flow{{Product: GreenPowerSlug, Rate: 1}},
	},
	{
		Name:     "Yellow Power Slug",
		Producer: Nature,
		Outputs:  []Flow{{Product: YellowPowerSlug, Rate: 1}},
	},
	{
		Name:     "Purple Power Slug",
		Producer: Nature,
		Outputs:  []Flow{{Product: PurplePowerSlug, Rate: 1}},
	},
	{
		Name:     "Flower Petals",
		Producer: Nature,
		Outputs:  []Flow{{Product: FlowerPetals, Rate: 1}},
	},
	{
		Name:     "Alien Carapace",
		Producer: Nature,
		Outputs:  []Flow{{Product: AlienCarapace, Rate: 1}},
	},
	{
		Name:     "Alien Organs",
		Producer: Nature,
		Outputs:  []Flow{{Product: AlienOrgans, Rate: 1}},
	},
	{
		Name:     "Coal",
		Producer: Miner,
		Outputs:  []Flow{{Product: Coal, Rate: Pure}},
	},
	{
		Name:     "Limestone",
		Producer: Miner,
		Outputs:  []Flow{{Product: Limestone, Rate: Pure}},
	},
	{
		Name:     "Bauxite",
		Producer: Miner,
		Outputs:  []Flow{{Product: Bauxite, Rate: Pure}},
	},
	{
		Name:     "Caterium Ore",
		Producer: Miner,
		Outputs:  []Flow{{Product: CateriumOre, Rate: Pure}},
	},
	{
		Name:     "Copper Ore",
		Producer: Miner,
		Outputs:  []Flow{{Product: CopperOre, Rate: Pure}},
	},
	{
		Name:     "Iron Ore",
		Producer: Miner,
		Outputs:  []Flow{{Product: IronOre, Rate: Pure}},
	},
	{
		Name:     "Uranium",
		Producer: Miner,
		Outputs:  []Flow{{Product: Uranium, Rate: Normal}},
	},
	{
		Name:     "Raw Quartz",
		Producer: Miner,
		Outputs:  []Flow{{Product: RawQuartz, Rate: Pure}},
	},
	{
		Name:     "Sulfur",
		Producer: Miner,
		Outputs:  []Flow{{Product: Sulfur, Rate: Normal}},
	},
	{
		Name:     "Crude Oil",
		Producer: OilExtractor,
		Outputs:  []Flow{{Product: CrudeOil, Rate: 240}},
	},
	{
		Name:     "Water",
		Producer: WaterExtractor,
		Outputs:  []Flow{{Product: Water, Rate: 120}},
	},
	{
		Name:     "Copper Ingot",
		Producer: Smelter,
		Inputs:   []Flow{{Product: CopperOre, Rate: 30}},
		Outputs:  []Flow{{Product: CopperIngot, Rate: 30}},
	},
	{
		Name:     "Iron Ingot",
		Producer: Smelter,
		Inputs:   []Flow{{Product: IronOre, Rate: 30}},
		Outputs:  []Flow{{Product: IronIngot, Rate: 30}},
	},
	{
		Name:     "Alternate: Pure Aluminum Ingot",
		Producer: Smelter,
		Inputs:   []Flow{{Product: AluminumScrap, Rate: 144}},
		Outputs:  []Flow{{Product: AluminumIngot, Rate: 36}},
	},
	{
		Name:     "Caterium Ingot",
		Producer: Smelter,
		Inputs:   []Flow{{Product: CateriumOre, Rate: 45}},
		Outputs:  []Flow{{Product: CateriumIngot, Rate: 15}},
	},
	{
		Name:     "Alternate: Coke Steel Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: IronOre, Rate: 75},
			{Product: PetroleumCoke, Rate: 75},
		},
		Outputs: []Flow{{Product: SteelIngot, Rate: 20}},
	},
	{
		Name:     "Alternate: Copper Allow Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: CopperOre, Rate: 50},
			{Product: IronOre, Rate: 25},
		},
		Outputs: []Flow{{Product: CopperIngot, Rate: 100}},
	},
	{
		Name:     "Aluminum Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: AluminumScrap, Rate: 240},
			{Product: Silica, Rate: 140},
		},
		Outputs: []Flow{{Product: AluminumIngot, Rate: 80}},
	},
	{
		Name:     "Steel Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: IronOre, Rate: 45},
			{Product: Coal, Rate: 45},
		},
		Outputs: []Flow{{Product: SteelIngot, Rate: 45}},
	},
	{
		Name:     "Alternate: Iron Alloy Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: IronOre, Rate: 20},
			{Product: CopperOre, Rate: 20},
		},
		Outputs: []Flow{{Product: IronIngot, Rate: 50}},
	},
	{
		Name:     "Alternate: Solid Steel Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: IronIngot, Rate: 40},
			{Product: Coal, Rate: 40},
		},
		Outputs: []Flow{{Product: SteelIngot, Rate: 60}},
	},
	{
		Name:     "Alternate: Compacted Steel Ingot",
		Producer: Foundry,
		Inputs: []Flow{
			{Product: IronOre, Rate: 22.5},
			{Product: CompactedCoal, Rate: 11.25},
		},
		Outputs: []Flow{{Product: SteelIngot, Rate: 37.5}},
	},
	{
		Name:     "Cable",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Wire, Rate: 60}},
		Outputs:  []Flow{{Product: Cable, Rate: 30}},
	},
	{
		Name:     "Wire",
		Producer: Constructor,
		Inputs:   []Flow{{Product: CopperIngot, Rate: 15}},
		Outputs:  []Flow{{Product: Wire, Rate: 30}},
	},
	{
		Name:     "Concrete",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Limestone, Rate: 45}},
		Outputs:  []Flow{{Product: Concrete, Rate: 15}},
	},
	{
		Name:     "Screw",
		Producer: Constructor,
		Inputs:   []Flow{{Product: IronRod, Rate: 10}},
		Outputs:  []Flow{{Product: Screw, Rate: 40}},
	},
	{
		Name:     "Biomass (Leaves)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Leaves, Rate: 120}},
		Outputs:  []Flow{{Product: Biomass, Rate: 60}},
	},
	{
		Name:     "Biomass (Wood)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Wood, Rate: 60}},
		Outputs:  []Flow{{Product: Biomass, Rate: 300}},
	},
	{
		Name:     "Iron Plate",
		Producer: Constructor,
		Inputs:   []Flow{{Product: IronIngot, Rate: 30}},
		Outputs:  []Flow{{Product: IronPlate, Rate: 20}},
	},
	{
		Name:     "Iron Rod",
		Producer: Constructor,
		Inputs:   []Flow{{Product: IronIngot, Rate: 15}},
		Outputs:  []Flow{{Product: IronRod, Rate: 15}},
	},
	{
		Name:     "Copper Sheet",
		Producer: Constructor,
		Inputs:   []Flow{{Product: CopperIngot, Rate: 20}},
		Outputs:  []Flow{{Product: CopperSheet, Rate: 10}},
	},
	{
		Name:     "Quartz Crystal",
		Producer: Constructor,
		Inputs:   []Flow{{Product: RawQuartz, Rate: 37.5}},
		Outputs:  []Flow{{Product: QuartzCrystal, Rate: 22.5}},
	},
	{
		Name:     "Alternate: Steel Rod",
		Producer: Constructor,
		Inputs:   []Flow{{Product: SteelIngot, Rate: 12}},
		Outputs:  []Flow{{Product: IronRod, Rate: 48}},
	},
	{
		Name:     "Steel Beam",
		Producer: Constructor,
		Inputs:   []Flow{{Product: SteelIngot, Rate: 60}},
		Outputs:  []Flow{{Product: SteelBeam, Rate: 15}},
	},
	{
		Name:     "Steel Pipe",
		Producer: Constructor,
		Inputs:   []Flow{{Product: SteelIngot, Rate: 30}},
		Outputs:  []Flow{{Product: SteelPipe, Rate: 20}},
	},
	{
		Name:     "Alternate: Charcoal",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Wood, Rate: 15}},
		Outputs:  []Flow{{Product: Coal, Rate: 150}},
	},
	{
		Name:     "Alternate: Biocoal",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Biomass, Rate: 37.5}},
		Outputs:  []Flow{{Product: Coal, Rate: 45}},
	},
	{
		Name:     "Alternate: Casted Screw",
		Producer: Constructor,
		Inputs:   []Flow{{Product: IronIngot, Rate: 12.5}},
		Outputs:  []Flow{{Product: Screw, Rate: 50}},
	},
	{
		Name:     "Alternate: Steel Screw",
		Producer: Constructor,
		Inputs:   []Flow{{Product: SteelBeam, Rate: 5}},
		Outputs:  []Flow{{Product: Screw, Rate: 260}},
	},
	{
		Name:     "Silica",
		Producer: Constructor,
		Inputs:   []Flow{{Product: RawQuartz, Rate: 22.5}},
		Outputs:  []Flow{{Product: Silica, Rate: 37.5}},
	},
	{
		Name:     "Alternate Iron Wire",
		Producer: Constructor,
		Inputs:   []Flow{{Product: IronIngot, Rate: 12.5}},
		Outputs:  []Flow{{Product: Wire, Rate: 22.5}},
	},
	{
		Name:     "Alternate: Caterium Wire",
		Producer: Constructor,
		Inputs:   []Flow{{Product: CateriumIngot, Rate: 15}},
		Outputs:  []Flow{{Product: Wire, Rate: 120}},
	},
	{
		Name:     "Solid Biofuel",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Biomass, Rate: 120}},
		Outputs:  []Flow{{Product: SolidBiofuel, Rate: 60}},
	},
	{
		Name:     "Empty Canister",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Plastic, Rate: 30}},
		Outputs:  []Flow{{Product: EmptyCanister, Rate: 60}},
	},
	{
		Name:     "Biomass (Alien Carapace)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: AlienCarapace, Rate: 15}},
		Outputs:  []Flow{{Product: Biomass, Rate: 1500}},
	},
	{
		Name:     "Spiked Rebar",
		Producer: Constructor,
		Inputs:   []Flow{{Product: IronRod, Rate: 15}},
		Outputs:  []Flow{{Product: SpikedRebar, Rate: 15}},
	},
	{
		Name:     "Biomass (Alien Organs",
		Producer: Constructor,
		Inputs:   []Flow{{Product: AlienOrgans, Rate: 7.5}},
		Outputs:  []Flow{{Product: Biomass, Rate: 1500}},
	},
	{
		Name:     "Quickwire",
		Producer: Constructor,
		Inputs:   []Flow{{Product: CateriumIngot, Rate: 12}},
		Outputs:  []Flow{{Product: Quickwire, Rate: 60}},
	},
	{
		Name:     "Color Cartridge",
		Producer: Constructor,
		Inputs:   []Flow{{Product: FlowerPetals, Rate: 37.5}},
		Outputs:  []Flow{{Product: ColorCartridge, Rate: 75}},
	},
	{
		Name:     "Biomass (Mycelia)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: Mycelia, Rate: 150}},
		Outputs:  []Flow{{Product: Biomass, Rate: 150}},
	},
	{
		Name:     "Power Shard (1)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: GreenPowerSlug, Rate: 7.5}},
		Outputs:  []Flow{{Product: PowerShard, Rate: 7.5}},
	},
	{
		Name:     "Power Shard (2)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: YellowPowerSlug, Rate: 5}},
		Outputs:  []Flow{{Product: PowerShard, Rate: 10}},
	},
	{
		Name:     "Power Shard (5)",
		Producer: Constructor,
		Inputs:   []Flow{{Product: PurplePowerSlug, Rate: 2.5}},
		Outputs:  []Flow{{Product: PowerShard, Rate: 12.5}},
	},

	{
		Name:     "Reinforced Iron Plate",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: IronPlate, Rate: 30},
			{Product: Screw, Rate: 60},
		},
		Outputs: []Flow{{Product: ReinforcedIronPlate, Rate: 5}},
	},
	{
		Name:     "Alternate: Adhered Iron Plate",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: IronPlate, Rate: 11.25},
			{Product: Rubber, Rate: 3.75},
		},
		Outputs: []Flow{{Product: ReinforcedIronPlate, Rate: 3.75}},
	},
	{
		Name:     "Circuit Board",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CopperSheet, Rate: 15},
			{Product: Plastic, Rate: 30},
		},
		Outputs: []Flow{{Product: CircuitBoard, Rate: 7.5}},
	},
	{
		Name:     "Alternate: Bolted Frame",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: ReinforcedIronPlate, Rate: 7.5},
			{Product: Screw, Rate: 140},
		},
		Outputs: []Flow{{Product: ModularFrame, Rate: 5}},
	},
	{
		Name:     "Modular Frame",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: ReinforcedIronPlate, Rate: 3},
			{Product: IronRod, Rate: 12},
		},
		Outputs: []Flow{{Product: ModularFrame, Rate: 2}},
	},
	{
		Name:     "Rotor",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: IronRod, Rate: 20},
			{Product: Screw, Rate: 100},
		},
		Outputs: []Flow{{Product: Rotor, Rate: 4}},
	},
	{
		Name:     "Smart Plating",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: ReinforcedIronPlate, Rate: 2},
			{Product: Rotor, Rate: 2},
		},
		Outputs: []Flow{{Product: SmartPlating, Rate: 2}},
	},
	{
		Name:     "Alternate: Coated Iron Plate",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: IronIngot, Rate: 50},
			{Product: Plastic, Rate: 10},
		},
		Outputs: []Flow{{Product: IronPlate, Rate: 75}},
	},
	{
		Name:     "Alternate: Copper Rotor",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CopperSheet, Rate: 22.5},
			{Product: Screw, Rate: 195},
		},
		Outputs: []Flow{{Product: Rotor, Rate: 11.25}},
	},
	{
		Name:     "Alclad Aluminum Sheet",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: AluminumIngot, Rate: 60},
			{Product: CopperIngot, Rate: 22.5},
		},
		Outputs: []Flow{{Product: AlcladAluminumSheet, Rate: 30}},
	},
	{
		Name:     "Alternate: Electrode Circuit Board",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Rubber, Rate: 30},
			{Product: PetroleumCoke, Rate: 45},
		},
		Outputs: []Flow{{Product: CircuitBoard, Rate: 5}},
	},
	{
		Name:     "Alternate: Fused Wire",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CopperIngot, Rate: 12},
			{Product: CateriumIngot, Rate: 3},
		},
		Outputs: []Flow{{Product: Wire, Rate: 90}},
	},
	{
		Name:     "Encased Industrial Beam",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: SteelBeam, Rate: 24},
			{Product: Concrete, Rate: 30},
		},
		Outputs: []Flow{{Product: EncasedIndustrialBeam, Rate: 6}},
	},
	{
		Name:     "Motor",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Rotor, Rate: 10},
			{Product: Stator, Rate: 10},
		},
		Outputs: []Flow{{Product: Motor, Rate: 5}},
	},
	{
		Name:     "Stator",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: SteelPipe, Rate: 15},
			{Product: Wire, Rate: 40},
		},
		Outputs: []Flow{{Product: Stator, Rate: 5}},
	},
	{
		Name:     "Automated Wiring",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Stator, Rate: 2.5},
			{Product: Cable, Rate: 50},
		},
		Outputs: []Flow{{Product: AutomatedWiring, Rate: 2.5}},
	},
	{
		Name:     "A.I. Limiter",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CopperSheet, Rate: 25},
			{Product: Quickwire, Rate: 100},
		},
		Outputs: []Flow{{Product: AILimiter, Rate: 5}},
	},
	{
		Name:     "Fabric",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Mycelia, Rate: 15},
			{Product: Biomass, Rate: 75},
		},
		Outputs: []Flow{{Product: Fabric, Rate: 15}},
	},
	{
		Name:     "Alternate: Rubber Concrete",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Limestone, Rate: 50},
			{Product: Rubber, Rate: 10},
		},
		Outputs: []Flow{{Product: Concrete, Rate: 45}},
	},
	{
		Name:     "Alternate: Steel Coated Plate",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: SteelIngot, Rate: 7.5},
			{Product: Plastic, Rate: 5},
		},
		Outputs: []Flow{{Product: IronPlate, Rate: 45}},
	},
	{
		Name:     "Alternate: Versatile Framework",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: ModularFrame, Rate: 2.5},
			{Product: SteelBeam, Rate: 30},
		},
		Outputs: []Flow{{Product: VersatileFramework, Rate: 5}},
	},
	{
		Name:     "Alternate: Compacted Coal",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Coal, Rate: 25},
			{Product: Sulfur, Rate: 25},
		},
		Outputs: []Flow{{Product: CompactedCoal, Rate: 25}},
	},
	{
		Name:     "Alternate: Insulated Cable",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Wire, Rate: 45},
			{Product: Rubber, Rate: 30},
		},
		Outputs: []Flow{{Product: Cable, Rate: 100}},
	},
	{
		Name:     "Alternate: Quickwire Cable",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Quickwire, Rate: 7.5},
			{Product: Rubber, Rate: 5},
		},
		Outputs: []Flow{{Product: Cable, Rate: 27.5}},
	},
	{
		Name:     "Alternate: Silicone Circuit Board",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CopperSheet, Rate: 27.5},
			{Product: Silica, Rate: 27.5},
		},
		Outputs: []Flow{{Product: CircuitBoard, Rate: 12.5}},
	},
	{
		Name:     "Alternate: Caterium Circuit Board",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Plastic, Rate: 12.5},
			{Product: Quickwire, Rate: 37.5},
		},
		Outputs: []Flow{{Product: CircuitBoard, Rate: 8.75}},
	},
	{
		Name:     "Alternate: Crystal Computer",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CircuitBoard, Rate: 7.5},
			{Product: CrystalOscillator, Rate: 2.8125},
		},
		Outputs: []Flow{{Product: Computer, Rate: 2.8125}},
	},
	{
		Name:     "Alternate: Fine Concrete",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Silica, Rate: 7.5},
			{Product: Limestone, Rate: 30},
		},
		Outputs: []Flow{{Product: Concrete, Rate: 25}},
	},
	{
		Name:     "Alternate: Electromagnetic Control Rod",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Stator, Rate: 6},
			{Product: AILimiter, Rate: 4},
		},
		Outputs: []Flow{{Product: ElectromagneticControlRod, Rate: 4}},
	},
	{
		Name:     "Encased Uranium Cell",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: UraniumPellet, Rate: 40},
			{Product: Concrete, Rate: 9},
		},
		Outputs: []Flow{{Product: EncasedUraniumCell, Rate: 10}},
	},
	{
		Name:     "Alternate: Fine Black Powder",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Sulfur, Rate: 7.5},
			{Product: CompactedCoal, Rate: 3.75},
		},
		Outputs: []Flow{{Product: BlackPowder, Rate: 15}},
	},
	{
		Name:     "Alternate: Heat Exchanger",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: AlcladAluminumSheet, Rate: 37.5},
			{Product: CopperSheet, Rate: 56.25},
		},
		Outputs: []Flow{{Product: HeatSink, Rate: 13.125}},
	},
	{
		Name:     "Alternate: Heat Sink",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: AlcladAluminumSheet, Rate: 40},
			{Product: Rubber, Rate: 70},
		},
		Outputs: []Flow{{Product: HeatSink, Rate: 10}},
	},
	{
		Name:     "Alternate: Steeled Frame",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: ReinforcedIronPlate, Rate: 2},
			{Product: SteelPipe, Rate: 10},
		},
		Outputs: []Flow{{Product: ModularFrame, Rate: 3}},
	},
	{
		Name:     "Nobelisk",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: BlackPowder, Rate: 15},
			{Product: SteelPipe, Rate: 30},
		},
		Outputs: []Flow{{Product: Nobelisk, Rate: 3}},
	},
	{
		Name:     "Alternate: Fused Quickwire",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: CateriumIngot, Rate: 7.5},
			{Product: CopperIngot, Rate: 37.5},
		},
		Outputs: []Flow{{Product: Quickwire, Rate: 90}},
	},
	{
		Name:     "Alternate: Bolted Iron Plate",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: IronPlate, Rate: 90},
			{Product: Screw, Rate: 250},
		},
		Outputs: []Flow{{Product: ReinforcedIronPlate, Rate: 15}},
	},
	{
		Name:     "Alternate: Stitched Iron Plate",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: IronPlate, Rate: 18.75},
			{Product: Wire, Rate: 37.5},
		},
		Outputs: []Flow{{Product: ReinforcedIronPlate, Rate: 5.625}},
	},
	{
		Name:     "Alternate: Encased Industrial Pipe",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: SteelPipe, Rate: 28},
			{Product: Concrete, Rate: 20},
		},
		Outputs: []Flow{{Product: EncasedIndustrialBeam, Rate: 4}},
	},
	{
		Name:     "Alternate: Steel Rotor",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: SteelPipe, Rate: 10},
			{Product: Wire, Rate: 30},
		},
		Outputs: []Flow{{Product: Rotor, Rate: 5}},
	},
	{
		Name:     "Alternate: Cheap Silica",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: RawQuartz, Rate: 11.25},
			{Product: Limestone, Rate: 18.75},
		},
		Outputs: []Flow{{Product: Silica, Rate: 26.25}},
	},
	{
		Name:     "Alternate: Quickwire Stator",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: SteelPipe, Rate: 16},
			{Product: Quickwire, Rate: 60},
		},
		Outputs: []Flow{{Product: Stator, Rate: 8}},
	},
	{
		Name:     "Black Powder",
		Producer: Assembler,
		Inputs: []Flow{
			{Product: Coal, Rate: 7.5},
			{Product: Sulfur, Rate: 15},
		},
		Outputs: []Flow{{Product: BlackPowder, Rate: 7.5}},
	},

	{
		Name:     "Alternate: Flexible Framework",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: ModularFrame, Rate: 3.75},
			{Product: SteelBeam, Rate: 22.5},
			{Product: Rubber, Rate: 30},
		},
		Outputs: []Flow{{Product: VersatileFramework, Rate: 7.5}},
	},
	{
		Name:     "Alternate: Heavy Flexible Frame",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: ModularFrame, Rate: 18.75},
			{Product: EncasedIndustrialBeam, Rate: 11.25},
			{Product: Rubber, Rate: 75},
			{Product: Screw, Rate: 390},
		},
		Outputs: []Flow{{Product: HeavyModularFrame, Rate: 3.75}},
	},
	{
		Name:     "Computer",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: CircuitBoard, Rate: 25},
			{Product: Cable, Rate: 22.5},
			{Product: Plastic, Rate: 45},
			{Product: Screw, Rate: 130},
		},
		Outputs: []Flow{{Product: Computer, Rate: 2.5}},
	},
	{
		Name:     "Modular Engine",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Motor, Rate: 2},
			{Product: Rubber, Rate: 15},
			{Product: SmartPlating, Rate: 2},
		},
		Outputs: []Flow{{Product: ModularEngine, Rate: 1}},
	},
	{
		Name:     "Adaptive Control Unit",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: AutomatedWiring, Rate: 7.5},
			{Product: CircuitBoard, Rate: 5},
			{Product: HeavyModularFrame, Rate: 1},
			{Product: Computer, Rate: 1},
		},
		Outputs: []Flow{{Product: AdaptiveControlUnit, Rate: 1}},
	},
	{
		Name:     "Alternate: Automated Speed Wiring",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Stator, Rate: 3.75},
			{Product: Wire, Rate: 75},
			{Product: HighSpeedConnector, Rate: 1.875},
		},
		Outputs: []Flow{{Product: AutomatedWiring, Rate: 7.5}},
	},
	{
		Name:     "Heavy Modular Frame",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: ModularFrame, Rate: 10},
			{Product: SteelPipe, Rate: 30},
			{Product: EncasedIndustrialBeam, Rate: 10},
			{Product: Screw, Rate: 200},
		},
		Outputs: []Flow{{Product: HeavyModularFrame, Rate: 2}},
	},
	{
		Name:     "Alternate: Plastic Smart Plating",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: ReinforcedIronPlate, Rate: 2.5},
			{Product: Rotor, Rate: 2.5},
			{Product: Plastic, Rate: 7.5},
		},
		Outputs: []Flow{{Product: SmartPlating, Rate: 5}},
	},
	{
		Name:     "Alternate: Crystal Beacon",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: SteelBeam, Rate: 2},
			{Product: SteelPipe, Rate: 8},
			{Product: CrystalOscillator, Rate: 0.5},
		},
		Outputs: []Flow{{Product: Beacon, Rate: 20}},
	},
	{
		Name:     "Alternate: Caterium Computer",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: CircuitBoard, Rate: 26.25},
			{Product: Quickwire, Rate: 105},
			{Product: Rubber, Rate: 45},
		},
		Outputs: []Flow{{Product: Computer, Rate: 3.75}},
	},
	{
		Name:     "Alternate: Insulated Crystal Oscillator",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: QuartzCrystal, Rate: 18.75},
			{Product: Rubber, Rate: 13.125},
			{Product: AILimiter, Rate: 1.875},
		},
		Outputs: []Flow{{Product: CrystalOscillator, Rate: 1.875}},
	},
	{
		Name:     "Crystal Oscillator",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: QuartzCrystal, Rate: 18},
			{Product: Cable, Rate: 14},
			{Product: ReinforcedIronPlate, Rate: 2.5},
		},
		Outputs: []Flow{{Product: CrystalOscillator, Rate: 1}},
	},
	{
		Name:     "Nuclear Fuel Rod",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: EncasedUraniumCell, Rate: 10},
			{Product: EncasedIndustrialBeam, Rate: 1.2},
			{Product: ElectromagneticControlRod, Rate: 2},
		},
		Outputs: []Flow{{Product: NuclearFuelRod, Rate: 0.4}},
	},
	{
		Name:     "Battery",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: AlcladAluminumSheet, Rate: 15},
			{Product: Wire, Rate: 30},
			{Product: Sulfur, Rate: 37.5},
			{Product: Plastic, Rate: 15},
		},
		Outputs: []Flow{{Product: Battery, Rate: 5.625}},
	},
	{
		Name:     "Turbo Motor",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: HeatSink, Rate: 7.5},
			{Product: RadioControlUnit, Rate: 3.75},
			{Product: Motor, Rate: 7.5},
			{Product: Rubber, Rate: 45},
		},
		Outputs: []Flow{{Product: TurboMotor, Rate: 1.875}},
	},
	{
		Name:     "Alternate: Heavy Encased Frame",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: ModularFrame, Rate: 7.5},
			{Product: EncasedIndustrialBeam, Rate: 9.375},
			{Product: SteelPipe, Rate: 33.75},
			{Product: Concrete, Rate: 20.625},
		},
		Outputs: []Flow{{Product: HeavyModularFrame, Rate: 2.8125}},
	},
	{
		Name:     "Alternate: Silicone High-Speed Connector",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Quickwire, Rate: 90},
			{Product: Silica, Rate: 37.5},
			{Product: CircuitBoard, Rate: 3},
		},
		Outputs: []Flow{{Product: HighSpeedConnector, Rate: 3}},
	},
	{
		Name:     "High Speed Connector",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Quickwire, Rate: 210},
			{Product: Cable, Rate: 37.5},
			{Product: CircuitBoard, Rate: 3.75},
		},
		Outputs: []Flow{{Product: HighSpeedConnector, Rate: 3.75}},
	},
	{
		Name:     "Alternate: Rigour Motor",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Rotor, Rate: 3.75},
			{Product: Stator, Rate: 3.75},
			{Product: CrystalOscillator, Rate: 1.25},
		},
		Outputs: []Flow{{Product: Motor, Rate: 7.5}},
	},
	{
		Name:     "Alternate: Seismic Nobelisk",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: BlackPowder, Rate: 12},
			{Product: SteelPipe, Rate: 12},
			{Product: CrystalOscillator, Rate: 1.5},
		},
		Outputs: []Flow{{Product: Nobelisk, Rate: 6}},
	},
	{
		Name:     "Alternate: Nuclear Fuel Unit",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: EncasedUraniumCell, Rate: 10},
			{Product: ElectromagneticControlRod, Rate: 2},
			{Product: CrystalOscillator, Rate: 0.6},
			{Product: Beacon, Rate: 1.2},
		},
		Outputs: []Flow{{Product: NuclearFuelRod, Rate: 0.6}},
	},
	{
		Name:     "Alternate: Radio Control System",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: HeatSink, Rate: 12.5},
			{Product: Supercomputer, Rate: 1.25},
			{Product: QuartzCrystal, Rate: 37.5},
		},
		Outputs: []Flow{{Product: RadioControlUnit, Rate: 3.75}},
	},
	{
		Name:     "Radio Control Unit",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: HeatSink, Rate: 10},
			{Product: Rubber, Rate: 40},
			{Product: CrystalOscillator, Rate: 2.5},
			{Product: Computer, Rate: 2.5},
		},
		Outputs: []Flow{{Product: RadioControlUnit, Rate: 2.5}},
	},
	{
		Name:     "Alternate: Turbo Rigour Motor",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Motor, Rate: 6.5625},
			{Product: RadioControlUnit, Rate: 4.6875},
			{Product: AILimiter, Rate: 8.4375},
			{Product: Stator, Rate: 6.5625},
		},
		Outputs: []Flow{{Product: TurboMotor, Rate: 2.8125}},
	},
	{
		Name:     "Alternate: Infused Uranium Cell",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: UraniumPellet, Rate: 20},
			{Product: Sulfur, Rate: 22.5},
			{Product: Silica, Rate: 22.5},
			{Product: Quickwire, Rate: 37.5},
		},
		Outputs: []Flow{{Product: EncasedUraniumCell, Rate: 17.5}},
	},
	{
		Name:     "Beacon",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: IronPlate, Rate: 22.5},
			{Product: IronRod, Rate: 7.5},
			{Product: Wire, Rate: 112.5},
			{Product: Cable, Rate: 15},
		},
		Outputs: []Flow{{Product: Beacon, Rate: 7.5}},
	},
	{
		Name:     "Gas Filter",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Coal, Rate: 37.5},
			{Product: Rubber, Rate: 15},
			{Product: Fabric, Rate: 15},
		},
		Outputs: []Flow{{Product: GasFilter, Rate: 7.5}},
	},
	{
		Name:     "Iodine Infused Filter",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: GasFilter, Rate: 3.75},
			{Product: Quickwire, Rate: 30},
			{Product: Rubber, Rate: 7.5},
		},
		Outputs: []Flow{{Product: IodineInfusedFilter, Rate: 3.75}},
	},
	{
		Name:     "Supercomputer",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Computer, Rate: 3.75},
			{Product: AILimiter, Rate: 3.75},
			{Product: HighSpeedConnector, Rate: 5.625},
			{Product: Plastic, Rate: 52.5},
		},
		Outputs: []Flow{{Product: Supercomputer, Rate: 1.875}},
	},
	{
		Name:     "Rifle Cartridge",
		Producer: Manufacturer,
		Inputs: []Flow{
			{Product: Beacon, Rate: 3},
			{Product: SteelPipe, Rate: 30},
			{Product: BlackPowder, Rate: 30},
			{Product: Rubber, Rate: 30},
		},
		Outputs: []Flow{{Product: RifleCartridge, Rate: 15}},
	},

	{
		Name:     "Fuel",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CrudeOil, Rate: 60},
		},
		Outputs: []Flow{
			{Product: Fuel, Rate: 40},
			{Product: PolymerResin, Rate: 30},
		},
	},
	{
		Name:     "Petroleum Coke",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: HeavyOilResidue, Rate: 40},
		},
		Outputs: []Flow{
			{Product: PetroleumCoke, Rate: 120},
		},
	},
	{
		Name:     "Plastic",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CrudeOil, Rate: 30},
		},
		Outputs: []Flow{
			{Product: Plastic, Rate: 20},
			{Product: HeavyOilResidue, Rate: 10},
		},
	},
	{
		Name:     "Rubber",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CrudeOil, Rate: 30},
		},
		Outputs: []Flow{
			{Product: Rubber, Rate: 20},
			{Product: HeavyOilResidue, Rate: 20},
		},
	},
	{
		Name:     "Residual Fuel",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: HeavyOilResidue, Rate: 60},
		},
		Outputs: []Flow{
			{Product: Fuel, Rate: 40},
		},
	},
	{
		Name:     "Residual Plastic",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: PolymerResin, Rate: 60},
			{Product: Water, Rate: 20},
		},
		Outputs: []Flow{
			{Product: Plastic, Rate: 20},
		},
	},
	{
		Name:     "Residual Rubber",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: PolymerResin, Rate: 40},
			{Product: Water, Rate: 40},
		},
		Outputs: []Flow{
			{Product: Rubber, Rate: 20},
		},
	},
	{
		Name:     "Alternate: Coated Cable",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Wire, Rate: 37.5},
			{Product: HeavyOilResidue, Rate: 15},
		},
		Outputs: []Flow{
			{Product: Cable, Rate: 67.5},
		},
	},
	{
		Name:     "Alternate: Electrode - Aluminum Scrap",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: AluminaSolution, Rate: 90},
			{Product: Coal, Rate: 30},
		},
		Outputs: []Flow{
			{Product: AluminumScrap, Rate: 150},
			{Product: Water, Rate: 30},
		},
	},
	{
		Name:     "Alumina Solution",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Bauxite, Rate: 70},
			{Product: Water, Rate: 100},
		},
		Outputs: []Flow{
			{Product: AluminaSolution, Rate: 80},
			{Product: Silica, Rate: 20},
		},
	},
	{
		Name:     "Aluminum Scrap",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: AluminaSolution, Rate: 240},
			{Product: PetroleumCoke, Rate: 60},
		},
		Outputs: []Flow{
			{Product: AluminumScrap, Rate: 360},
			{Product: Water, Rate: 60},
		},
	},
	{
		Name:     "Alternate: Heavy Oil Residue",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CrudeOil, Rate: 30},
		},
		Outputs: []Flow{
			{Product: HeavyOilResidue, Rate: 40},
			{Product: PolymerResin, Rate: 20},
		},
	},
	{
		Name:     "Alternate: Polyester Fabric",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: PolymerResin, Rate: 80},
			{Product: Water, Rate: 50},
		},
		Outputs: []Flow{
			{Product: Fabric, Rate: 5},
		},
	},
	{
		Name:     "Alternate: Polymer Resin",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CrudeOil, Rate: 60},
		},
		Outputs: []Flow{
			{Product: PolymerResin, Rate: 130},
			{Product: HeavyOilResidue, Rate: 20},
		},
	},
	{
		Name:     "Alternate: Pure Caterium Ingot",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CateriumOre, Rate: 24},
			{Product: Water, Rate: 24},
		},
		Outputs: []Flow{
			{Product: CateriumIngot, Rate: 12},
		},
	},
	{
		Name:     "Alternate: Pure Copper Ingot",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CopperOre, Rate: 15},
			{Product: Water, Rate: 10},
		},
		Outputs: []Flow{
			{Product: CopperIngot, Rate: 37.5},
		},
	},
	{
		Name:     "Alternate: Pure Iron Ingot",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: IronOre, Rate: 35},
			{Product: Water, Rate: 20},
		},
		Outputs: []Flow{
			{Product: IronOre, Rate: 65},
		},
	},
	{
		Name:     "Alternate: Pure Quartz Crystal",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: RawQuartz, Rate: 67.5},
			{Product: Water, Rate: 37.5},
		},
		Outputs: []Flow{
			{Product: QuartzCrystal, Rate: 52.5},
		},
	},
	{
		Name:     "Alternate: Recycled Rubber",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Plastic, Rate: 30},
			{Product: Fuel, Rate: 30},
		},
		Outputs: []Flow{
			{Product: Rubber, Rate: 60},
		},
	},
	{
		Name:     "Alternate: Steamed Copper Sheet",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: CopperIngot, Rate: 22.5},
			{Product: Water, Rate: 22.5},
		},
		Outputs: []Flow{
			{Product: CopperSheet, Rate: 22.5},
		},
	},
	{
		Name:     "Alternate: Turbo Heavy Fuel",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: HeavyOilResidue, Rate: 37.5},
			{Product: CompactedCoal, Rate: 30},
		},
		Outputs: []Flow{
			{Product: Turbofuel, Rate: 30},
		},
	},
	{
		Name:     "Packaged Turbofuel",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Turbofuel, Rate: 20},
			{Product: EmptyCanister, Rate: 20},
		},
		Outputs: []Flow{
			{Product: PackagedTurbofuel, Rate: 20},
		},
	},
	{
		Name:     "Alternate: Wet Concrete",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Limestone, Rate: 120},
			{Product: Water, Rate: 100},
		},
		Outputs: []Flow{
			{Product: Concrete, Rate: 80},
		},
	},
	{
		Name:     "Sulfuric Acid",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Sulfur, Rate: 50},
			{Product: Water, Rate: 50},
		},
		Outputs: []Flow{
			{Product: SulfuricAcid, Rate: 100},
		},
	},
	{
		Name:     "Uranium Pellet",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Uranium, Rate: 50},
			{Product: SulfuricAcid, Rate: 80},
		},
		Outputs: []Flow{
			{Product: UraniumPellet, Rate: 50},
			{Product: SulfuricAcid, Rate: 20},
		},
	},
	{
		Name:     "Alternate: Recycled Plastic",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Rubber, Rate: 30},
			{Product: Fuel, Rate: 30},
		},
		Outputs: []Flow{
			{Product: Plastic, Rate: 60},
		},
	},
	{
		Name:     "Turbofuel",
		Producer: Refinery,
		Inputs: []Flow{
			{Product: Fuel, Rate: 22.5},
			{Product: CompactedCoal, Rate: 15},
		},
		Outputs: []Flow{
			{Product: Turbofuel, Rate: 18.75},
		},
	},
}
