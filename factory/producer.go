package factory

// A Producer produces Product types.
type Producer struct {
	Name  string
	Power int
	Order int
}

// Producers
//nolint:gomnd
var (
	Nature         = Producer{Order: 0, Name: "Nature", Power: 0}
	OilExtractor   = Producer{Order: 1, Name: "Oil Extractor", Power: 40}
	WaterExtractor = Producer{Order: 2, Name: "Water Extractor", Power: 20}
	Miner          = Producer{Order: 3, Name: "Miner Mk. 3", Power: 30}
	Smelter        = Producer{Order: 4, Name: "Smelter", Power: 4}
	Foundry        = Producer{Order: 5, Name: "Foundry", Power: 16}
	Refinery       = Producer{Order: 6, Name: "Refinery", Power: 30}
	Constructor    = Producer{Order: 7, Name: "Constructor", Power: 4}
	Assembler      = Producer{Order: 8, Name: "Assembler", Power: 15}
	Manufacturer   = Producer{Order: 9, Name: "Manufacturer", Power: 55}
)
