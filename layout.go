package main

import (
	"fmt"

	"bitbucket.org/idomdavis/satisfactory/factory"
)

func main() {
	p := factory.Produce([]factory.Product{
		factory.PackagedTurbofuel,
		factory.TurboMotor,
		factory.AdaptiveControlUnit,
		factory.IodineInfusedFilter,
		factory.VersatileFramework,
		factory.ModularEngine,
		factory.Nobelisk,
		factory.Battery,
		factory.RifleCartridge,
	}, factory.NonOrganic{Comparator: factory.Fastest{}})

	lanes := p.Lanes()

	fmt.Println(p.String())
	fmt.Println(p.Power(), "KW")
	fmt.Println(len(lanes), "Lanes")

	for _, lane := range lanes {
		fmt.Println(lane)
	}
}
